import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { BarMenuPage } from '../bar-menu/bar-menu';
import { ReservationPage } from '../reservation/reservation';
import { WelcomePage } from '../welcome/welcome';

import { Song } from '../../models/song';
import { QueueProvider } from '../../providers/queue/queue';

import { SpotifyProvider } from '../../providers/social/spotify';
import { StorageProvider } from '../../providers/storage/storage';

import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-queue',
  templateUrl: 'queue.html'
})
export class QueuePage {

  constructor(
    public navCtrl: NavController,
    private queueProvider: QueueProvider,
    private spotifyProvider: SpotifyProvider,
    public alertCtrl: AlertController,
    private storageProvider: StorageProvider,
    private authProvider: AuthProvider,
    private toastCtrl: ToastController) {

    this.activePage = 1;
    this.menuIsOpen = false;

  }

  current: void | Song;
  queue: void | Song[];
  picks: void | Song[];
  activePage: number;
  menuIsOpen: boolean;

  viewQueue: boolean = true;
  viewPicker: boolean = false;

  ngOnInit (): void {
    this.queueProvider.getQueue ().subscribe ((queue) => {
      if (queue) {
        queue = queue.reverse ();
        this.current = queue.pop ();
        this.queue = queue.reverse();
      }
    });
  }

  private addToQueue(song: Song) {
    delete song.id;
    this.queueProvider.addSong (song).subscribe (() => {
      this.ngOnInit ();
      let toast = this.toastCtrl.create({
        message: 'Your song\'s registered, keep an eye on the queue',
        duration: 3000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
      this.toggleQueue();
    });
  }

  addSongToQueue(song: Song) {
    let confirm = this.alertCtrl.create({
      title: 'Add song to karaoke queue?',
      message: `Do you want to register to sing ${song.name}?`,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Register',
          handler: () => this.addToQueue(song)
        }
      ]
    });
    confirm.present();
  }

  toggleQueue () {
    this.viewPicker = false;
    this.viewQueue = true;
  }

  togglePicker () {
    if (this.spotifyProvider.getToken()) {
      this.spotifyProvider.getUserTracks().subscribe ((tracks) => {
        this.picks = tracks;
        this.viewQueue = false;
        this.viewPicker = true;
      });
    } else {
      this.authProvider.spotifySimpleAuth ().then (() => {
        this.spotifyProvider.getUserTracks().subscribe ((tracks) => {
          this.picks = tracks;
          this.viewQueue = false;
          this.viewPicker = true;
        });
      });
    }
  }

  doRefresh(refresher: any) {
    this.ngOnInit ();
    refresher.complete();
  }

  doPulling(refresher: any) {
    console.log('DOPULLING', refresher.progress);
  }

  toggleMenu() {
    this.menuIsOpen = !this.menuIsOpen;
  }

  navigateTo(index: number) {
    switch (index) {
      case 0:
        this.navCtrl.push(BarMenuPage);
        break;
      case 2:
        this.navCtrl.push(ReservationPage);
        break;
      case 4:
        this.storageProvider.clear();
        this.navCtrl.push(WelcomePage);
        break;
    }
  }

}
