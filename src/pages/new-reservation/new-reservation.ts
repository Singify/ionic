import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Reservation } from '../../models/reservation';
import { ReservationsProvider } from '../../providers/reservations/reservations';

import { IdentityProvider } from '../../providers/identity';

/**
 * Generated class for the NewReservationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-reservation',
  templateUrl: 'new-reservation.html',
})
export class NewReservationPage implements OnInit {

  id: number;
  reservation: Reservation;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private reservationProvider: ReservationsProvider,
    private identityProvider: IdentityProvider) {
  }

  ngOnInit() {
    this.reservation = new Reservation(0, "", "", 0, 0, Math.floor((Math.random() * 100) + 1), "", "", "", "", this.identityProvider.getName());
  }

  cancel(){
    this.navCtrl.pop();
  }

  addReservation() {
    var temp = this.reservation;
    delete temp.id;
    this.reservationProvider.createReservation (temp).subscribe((reservation) => {
      this.navParams.get('addReservation')(reservation);
      this.navCtrl.pop();
    });
  }

}
