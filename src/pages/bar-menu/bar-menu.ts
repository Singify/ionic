import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ReservationPage } from '../reservation/reservation';
import { QueuePage } from '../queue/queue';
import { WelcomePage } from '../welcome/welcome';

import { MenuItem } from '../../models/menu-item';

import { Order } from '../../models/order';

import { MenuProvider } from '../../providers/menu/menu';

import { StorageProvider } from '../../providers/storage/storage';

import { IdentityProvider } from '../../providers/identity';

@Component({
  selector: 'page-bar-menu',
  templateUrl: 'bar-menu.html'
})
export class BarMenuPage implements OnInit, OnDestroy {

  activeMenu: number;
  activePage: number;
  menuIsOpen: boolean;

  order: Order;
  items: MenuItem[];


  constructor(
    public navCtrl: NavController,
    private menuProvider: MenuProvider,
    public alertCtrl: AlertController,
    private storageProvider: StorageProvider,
    private identityProvider: IdentityProvider) {
    this.activeMenu = 0;
    this.activePage = 0;
    this.menuIsOpen = false;
  }

  ngOnInit (): void {

    this.menuProvider.getOrder ().subscribe((o) => {
      if (o.length > 0) {
        o = o[0];
        this.order = new Order(o.id, o.table, o.items, o.total, o.user, o.date);
        console.log(this.order);
      } else {
        return null;
      }
    });
    this.getItems ();
  }

  ngOnDestroy() {

  }

  doRefresh(refresher: any) {
    this.ngOnInit();
    refresher.complete();
  }

  doPulling(refresher: any) {
    console.log('DOPULLING', refresher.progress);
  }

  private addToCart(item: MenuItem) {
    let confirm = this.alertCtrl.create({
      title: `Add ${item.name} to order?`,
      message: `Do you want to order a ${item.name} for $${item.price}?`,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Order',
          handler: () => {
            console.log(this.order);
            if (this.order) {
              this.order.items.push (item);
              this.order.total += item.price;
              this.menuProvider.updateOrder (this.order.id, this.order).subscribe ((order) => {
                console.log(order);
                //this.order = order;
              });
            } else {
              let date = (new Date()).toISOString().substring(0, 10);
              return this.menuProvider.newOrder({
                table: Math.floor((Math.random() * 100) + 1),
                user: this.identityProvider.getName (),
                items: [item],
                total: item.price,
                date: date
              }).subscribe((o) => {
                this.order = new Order (o.id, o.table, o.items, o.total, o.user, o.date);
              });
            }
          }
        }
      ]
    });
    confirm.present();
  }

  toggleMenu() {
    this.menuIsOpen = !this.menuIsOpen;
  }

  getItems () {
    let type: string;

    if (this.activeMenu === 0) type = "Drink";
    if (this.activeMenu === 1) type = "Food";
    if (this.activeMenu === 2) type = "Snack";

    return this.menuProvider.getItems(type).subscribe((data) => {
      this.items = data;
    });
  }

  navigateTo(index: number) {
    switch (index) {
      case 1:
        this.navCtrl.push(QueuePage);
        break;
      case 2:
        this.navCtrl.push(ReservationPage);
        break;

      case 4:
        this.storageProvider.clear();
        this.navCtrl.push(WelcomePage);
        break;
    }
  }

  next () {
    this.activeMenu = this.activeMenu < 2 ? this.activeMenu + 1 : 0;
    this.getItems();
  }

  prev () {
    this.activeMenu = this.activeMenu > 0 ? this.activeMenu - 1 : 2;
    this.getItems();
  }


}
