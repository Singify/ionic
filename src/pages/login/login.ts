import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarMenuPage } from '../bar-menu/bar-menu';
import { AlertController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-login',
  templateUrl: './login.html'
})
export class LoginPage {

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public authProvider: AuthProvider) {

  }

  authenticate(provider: string) {
    // TODO: Perform authentication logic with auth service
    this.authProvider.authenticate(provider).then ( () => {
      this.navCtrl.push(BarMenuPage);
    }) ;
  }
}
