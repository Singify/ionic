import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { BarMenuPage } from '../bar-menu/bar-menu';

import { StorageProvider } from '../../providers/storage/storage';
import { IdentityProvider } from '../../providers/identity';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  template: `
    <ion-content>
      <ion-grid class="c-welcome o-brand-background">
        <ion-row align-items-end justify-content-end class="c-welcome__wrapper">

          <ion-col col-12 class="c-welcome__header">
            <span class="o-brand-title">Karaoke & Bar</span>
            <h1 class="u-font-display t4">What are we gonna sing today?</h1>
            <span class="o-brand-logo">Singify</span>
          </ion-col>

          <ion-col col-6 class="action">
            <div class="c-action-block">
              <button class="o-action" text-center (click)="handleNavigation()">Let's rock</button>
            </div>
          </ion-col>

        </ion-row>
      </ion-grid>
    </ion-content>
  `
})
export class WelcomePage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storageProvider: StorageProvider,
    private identityProvider: IdentityProvider) {
    storageProvider.get ("Username").then (function () {
      identityProvider.loadFromStorage ();
      navCtrl.push(BarMenuPage);
    }).catch (function () {

    });
  }

  private handleNavigation() {
    this.navCtrl.push(LoginPage);
  }

}
