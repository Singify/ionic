import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { BarMenuPage } from '../bar-menu/bar-menu';
import { QueuePage } from '../queue/queue';

import { WelcomePage } from '../welcome/welcome';
import { Reservation } from '../../models/reservation';
import { NewReservationPage } from '../new-reservation/new-reservation';
import { ReservationsProvider } from '../../providers/reservations/reservations';
import { StorageProvider } from '../../providers/storage/storage';

import { IdentityProvider } from '../../providers/identity';

@Component({
  selector: 'page-reservation',
  templateUrl: 'reservation.html'
})
export class ReservationPage {

  constructor(
    public navCtrl: NavController,
    private reservationProvider: ReservationsProvider,
    public alertCtrl: AlertController,
    private storageProvider: StorageProvider,
    private identityProvider: IdentityProvider) {
    this.activePage = 2;
    this.menuIsOpen = false;
  }

  current: void | Reservation;
  reservations: void | Reservation[];
  activePage: number;
  menuIsOpen: boolean;

  ngOnInit (): void {
    this.reservationProvider.getReservations (this.identityProvider.getName()).subscribe ((reservations) => {

      if (reservations) {
        reservations = reservations.reverse ();
        this.current = reservations.pop ();
        this.reservations = reservations.reverse();
      }

    });
  }

  toggleMenu() {
    this.menuIsOpen = !this.menuIsOpen;
  }

  doRefresh(refresher: any) {
    this.ngOnInit ();
    refresher.complete();
  }

  doPulling(refresher: any) {
    console.log('DOPULLING', refresher.progress);
  }

  toggleNewReservation() {
    this.navCtrl.push(NewReservationPage, {addReservation: this.addReservation.bind(this)});
  }

  handleDeletion(id: any) {
    let confirm = this.alertCtrl.create({
      title: 'You want to delete this reservation?',
      message: 'This action cannot be undone',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            if (this.reservations) {
              this.reservationProvider.deleteReservation (id).subscribe(() => {
                if (this.reservations) {
                  let index = this.reservations.findIndex((r) => r.id === id);
                  this.reservations = [...this.reservations.slice(0, index), ...this.reservations.slice(index + 1)]
                }

              });
            }
          }
        }
      ]
    });
    confirm.present();
  }

  addReservation(reservation: Reservation) {
    if (this.reservations) {
      this.reservations.push(reservation);
    } else {
      this.reservations = [reservation];
    }
  }

  navigateTo(index: number) {
    console.log(index);
    switch (index) {
      case 0:
        this.navCtrl.push(BarMenuPage);
        break;
      case 1:
        this.navCtrl.push(QueuePage);
        break;
      case 4:
        this.storageProvider.clear();
        this.navCtrl.push(WelcomePage);
        break;
    }
  }

}
