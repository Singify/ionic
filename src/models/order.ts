
import { MenuItem } from './menu-item';

export class Order {
  id: string;
  table: number;
  items: MenuItem[];
  total: number;
  user: string;
  date: string;
  constructor(
    id: string,
    table: number,
    items: MenuItem[],
    total: number,
    user: string,
    date: string
  ) {
    this.id = id;
    this.table = table;
    this.items = items;
    this.total = total;
    this.user = user;
    this.date = date;
  }
}
