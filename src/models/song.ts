export class Song {
  id: string | number;
  user: string;
  picture: string;
  name: string;
  artist: string;
  album: string;
  image: string;
  url: string;
  date: string;
  duration: number;

  constructor(
    id: number | string,
    user: string,
    picture: string,
    name: string,
    artist: string,
    album: string,
    image: string,
    url: string,
    date: string,
    duration: number
  ) {
    this.id = id;
    this.user = user;
    this.picture = picture;
    this.name = name;
    this.artist = artist;
    this.album = album;
    this.image = image;
    this.url = url;
    this.date = date;
    this.duration = duration;
  }

}
