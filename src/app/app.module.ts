import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { BarMenuPage } from '../pages/bar-menu/bar-menu';
import { ReservationPage } from '../pages/reservation/reservation';
import { NewReservationPage } from '../pages/new-reservation/new-reservation';
import { QueuePage } from '../pages/queue/queue';
import { WelcomePage } from '../pages/welcome/welcome';

import { MenuComponent } from '../components/menu/menu';

import { AuthProvider } from '../providers/auth/auth';
import { MenuProvider } from '../providers/menu/menu';
import { QueueProvider } from '../providers/queue/queue';
import { ReservationsProvider } from '../providers/reservations/reservations';
import { SpotifyProvider } from '../providers/social/spotify';
import { FacebookProvider } from '../providers/social/facebook';
import { StorageProvider } from '../providers/storage/storage';
import { IdentityProvider } from '../providers/identity';

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { SecureStorage } from '@ionic-native/secure-storage';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    BarMenuPage,
    ReservationPage,
    NewReservationPage,
    QueuePage,
    WelcomePage,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicPageModule.forChild(LoginPage),
    IonicPageModule.forChild(BarMenuPage),
    IonicPageModule.forChild(ReservationPage),
    IonicPageModule.forChild(QueuePage),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    BarMenuPage,
    ReservationPage,
    NewReservationPage,
    QueuePage,
    WelcomePage,
    MenuComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    MenuProvider,
    QueueProvider,
    ReservationsProvider,
    SpotifyProvider,
    FacebookProvider,
    InAppBrowser,
    SecureStorage,
    StorageProvider,
    IdentityProvider
  ]
})
export class AppModule {}
