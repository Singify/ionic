import { Injectable } from '@angular/core';

import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageProvider {

  constructor(private secureStorage: SecureStorage) {

  }

  public set (key, value) {
    return this.secureStorage.create('singify-storage').then((storage: SecureStorageObject) => {
      return storage.set (key, value);
    });
  }

  public get (key) {
    return this.secureStorage.create('singify-storage').then((storage: SecureStorageObject) => {
      return storage.get (key);
    });
  }

  public remove (key) {
    return this.secureStorage.create('singify-storage').then((storage: SecureStorageObject) => {
      return storage.remove(key)
    });
  }

  public clear () {
    return this.secureStorage.create('singify-storage').then((storage: SecureStorageObject) => {
      return storage.clear();
    });
  }
}
