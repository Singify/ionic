import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { MenuItem } from '../../models/menu-item';
import { Order } from '../../models/order';

import { ApiConf } from '../api-conf';

import { IdentityProvider } from '../identity';

/*
  Generated class for the MenuProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MenuProvider {

  order: Order;

  constructor(public http: Http, private identityProvider: IdentityProvider) {
    this.getOrder ();
  }

  getItems(type: String): Observable<MenuItem[]>{
    return this.http.get(`${ApiConf.api}/MenuItems?filter=%7B"where"%3A%7B"type"%3A"${type}"%7D%7D`)
      .map((response) => response.json())
      .map((response) => response.map((i) =>new MenuItem(i.id, i.name, i.description, i.price)));
  }

  getOrder () {
    let date = (new Date()).toISOString().substring(0, 10);
    return this.http.get(`${ApiConf.api}/Orders?filter=%7B"where"%3A%7B"user"%3A"${this.identityProvider.getName()}"%2C"date"%3A"${date}"%7D%7D`)
      .map((response) => response.json());
  }

  newOrder (order: any) {
    return this.http.post(`${ApiConf.api}/Orders`, order)
      .map((response) => response.json());
  }

  updateOrder (id: string, order: Order) {
    var tempOrder = Object.assign({}, order);
    delete tempOrder.id;
    return this.http.put(`${ApiConf.api}/Orders/${id}`, tempOrder)
    .map((response) => response.json());
  }
}
