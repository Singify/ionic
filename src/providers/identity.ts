import { Injectable } from '@angular/core';

import { StorageProvider } from './storage/storage';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IdentityProvider {

  private name: string;
  private picture: string;

  constructor(private secureStorage: StorageProvider) {
    this.name = '';
    this.picture = '';
  }

  loadFromStorage () {
    return this.secureStorage.get ("Username").then ((name) => {
      return this.secureStorage.get ("Picture").then ((picture) => {
        this.name = name;
        this.picture = picture;
      })
    })
  }

  getName () {
    return this.name;
  }

  getPicture () {
    return this.picture;
  }

  setName (name: string) {
    this.secureStorage.set ("Username", name);
    this.name = name;
  }

  setPicture (picture: string) {
    this.secureStorage.set ("Picture", picture);
    this.picture = picture;
  }

  public getInfo () {
    return {
      'name': this.name,
      'picture': this.picture
    };
  }
}
