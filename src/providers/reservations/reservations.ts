import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Reservation } from '../../models/reservation';

import { ApiConf } from '../api-conf';

import { StorageProvider } from '../storage/storage';

/*
  Generated class for the ReservationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReservationsProvider {

  constructor(public http: Http, storageProvider: StorageProvider) { }

  getReservations(username: string) {
    return this.http.get(`${ApiConf.api}/Reservations?filter=%7B"where"%3A%7B"user"%3A"${username}"%7D%7D`)
      .map((response) => response.json())
      .map((response) => response.map((r) =>
        new Reservation(
          r.id, r.name,
          r.description, r.guests,
          r.attending, r.table,
          r.event, r.date,
          r.time, r.service,
          r.user)));
  }

  createReservation (reservation: Reservation) {
    return this.http.post (`${ApiConf.api}/Reservations`, reservation)
    .map((response) => response.json ());
  }

  deleteReservation (id: any) {
    return this.http.delete (`${ApiConf.api}/Reservations/${id}`)  .map((response) => response.json ());
  }
}
