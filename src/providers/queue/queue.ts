import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Song } from '../../models/song';

import { ApiConf } from '../api-conf';

/*
  Generated class for the QueueProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QueueProvider {

  constructor(public http: Http) { }

  getQueue() {
    return this.http.get(`${ApiConf.api}/Songs`)
      .map((response) => response.json())
      .map((response) => response.map((i) =>
        new Song(i.id, i.user, i.picture, i.name, i.artist, i.album, i.image, i.url, i.date, i.duration)));
  }

  addSong (song: Song) {
    return this.http.post (`${ApiConf.api}/Songs`, song)
    .map((response) => response.json ());
  }
}
