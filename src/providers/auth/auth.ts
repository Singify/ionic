import { Injectable } from '@angular/core';

import { Http, Headers } from '@angular/http';
import { conf } from './auth.conf';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';



import { InAppBrowser } from '@ionic-native/in-app-browser';

import { StorageProvider } from '../storage/storage';
import { SpotifyProvider } from '../social/spotify';
import { FacebookProvider } from '../social/facebook';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor(public http: Http,
    private spotifyProvider: SpotifyProvider,
    private iab: InAppBrowser,
    private storageProvider: StorageProvider,
    private facebookProvider: FacebookProvider) {

  }

  private buildRoute (route: string, fields: any): string {
    let first = true;
    for (const field in fields) {
      if (first) {
        route += `?${field}=${fields[field]}`;
        first = false;
      } else {
        route += `&${field}=${fields[field]}`;
      }
    }
    return route;
  }

  private buildAuthRoute (service: string) {
    return this.buildRoute (conf[service].auth, conf[service].authFields);
  }

  spotifySimpleAuth () {
    return this.authRequest (this.buildAuthRoute ('spotify')).then ((token) => {
      this.spotifyProvider.setToken (token);
    });
  }

  authenticate (service: string) {
    if (service == "spotify") {
      return this.authRequest (this.buildAuthRoute ('spotify')).then ((token) => {
        this.storageProvider.set ("AccessToken", token);
        this.storageProvider.set ("SpotifyToken", token);

        this.spotifyProvider.setToken (token);
        this.spotifyProvider.getUserInfo ().subscribe (() => {

        });
        console.log(1);
      });
    } else if (service == "facebook") {
      return this.authRequest (this.buildAuthRoute ('facebook')).then ((token) => {
        this.storageProvider.set ("AccessToken", token);
        this.facebookProvider.setToken (token);
        this.facebookProvider.getUserInfo ().subscribe ((info) => {
          this.spotifyProvider.setInfo (info.username, info.picture);
        });
      });
    } else if (service == "twitter") {
      return this.accessRequest ('twitter', conf['twitter'].auth, conf['twitter'].authFields);
    } else if (service == "google") {

    } else if (service == "mastodon") {
      return this.authRequest (this.buildAuthRoute ('mastodon')).then ((token) => {
        console.log(token);
        this.storageProvider.set ("AccessToken", token);
      });
    } else {
      var tempName = "Anonymous-" + Math.floor((Math.random() * 500) + 1);
      this.storageProvider.set ("Username", tempName);
      this.storageProvider.set ("Picture", "assets/img/profile.png");
      this.spotifyProvider.setInfo (tempName, "assets/img/profile.png");
      return Promise.resolve();
    }

  }

  private accessRequest (service: string, route: string, fields: any): Promise<any> {
    console.log (fields);
    let headers = new Headers();
    console.log(fields);
    if (conf[service].basic) {
        headers.append('Authorization', `Basic ${conf[service].basic}`);
        headers.append ('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        console.log (headers);
    }
      console.log (headers);
    return this.http.post(route, fields, {headers: headers}).toPromise ();
  }

  private authRequest (route: string): Promise<any> {
    return new Promise((resolve, reject) => {
        var browserRef = this.iab.create (route, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
        browserRef.on ("loadstart").subscribe ((event) => {
            if ((event.url).indexOf("https://hyuchia.com/") === 0) {
                browserRef.close();
                console.log (event);
                var responseParameters = ((event.url).split("#")[1]).split("&");
                var parsedResponse = {};
                for (var i = 0; i < responseParameters.length; i++) {
                    parsedResponse[responseParameters[i].split("=")[0]] = responseParameters[i].split("=")[1];
                }
                if (parsedResponse["access_token"] !== undefined && parsedResponse["access_token"] !== null) {
                    resolve(parsedResponse["access_token"]);
                } else {
                    reject("Problem authenticating with Facebook");
                }
            }
        });

    });
  }
}
