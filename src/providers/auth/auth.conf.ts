export const conf = {
  'spotify': {
    'auth': 'https://accounts.spotify.com/authorize/',
    'token': 'https://accounts.spotify.com/api/token/',
    'basic': 'ZGJiYzJiZjYzM2MwNDNkMThmZjZjMzUwY2QyMmQxNjA6NTA1MTE5Zjg0MzgzNGU5YmFhZTU2OTVlNTk5MzRjZjA=',
    'authFields': {
      'client_id': 'dbbc2bf633c043d18ff6c350cd22d160',
      'response_type': 'token',
      'scope': 'playlist-read-private playlist-read-collaborative user-read-playback-state user-modify-playback-state user-library-read',
      'redirect_uri': 'https://hyuchia.com'
    },
    'tokenFields': {
      'grant_type': 'access_token',
      'redirect_uri': 'https://hyuchia.com'
    }
  },
  'facebook': {
    'auth': 'https://www.facebook.com/v2.11/dialog/oauth',
    'token': 'https://graph.facebook.com/v2.11/oauth/access_token',
    'authFields': {
      'client_id': '346323525817883',
      'redirect_uri': 'https://hyuchia.com',
      'response_type': 'token'
    },
    'tokenFields': {
      'client_id': '346323525817883',
      'client_secret': '113f2bad9a8a127d4ef360004f091f7d',
      'redirect_uri': 'https://hyuchia.com'
    }
  },
  'twitter': {
    'auth': 'https://api.twitter.com/oauth/authorize',
    'basic': 'IHJqMHBXc1dRbnNFSzdYVlJXSDBFMmF6TXM6Z25JeGN0ZDNralRYTUxxYWdrcml6Qml3STZETnJWSjh1Ukt1d1VEZHE1UlloTFY1ZnE=',
    'authFields': {
      'grant_type': 'client_credentials'
    }
  },
  'google': {
    'auth': 'https://accounts.google.com/o/oauth2/auth',
    'authFields': {
      'client_id': '419792952795-9f0rca9nbo3sse08um2m2vogqg23m5fj.apps.googleusercontent.com',
      'redirect_uri': 'https://hyuchia.com',
      'response_type': 'token',
      'scope': 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email'
    }
  },
  'mastodon': {
    'auth': 'https://mastodon.social/oauth/authorize',
    'authFields': {
      'client_id': '132629d99991029486dd5df24e300a9eb6c39cd537bbd9c1b09f8a0fece43509',
      'response_type': 'code',
      'redirect_uri': 'https://hyuchia.com'
    }
  }
};
