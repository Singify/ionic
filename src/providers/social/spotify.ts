import { Injectable } from '@angular/core';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Song } from '../../models/song';

import { IdentityProvider } from '../identity';

@Injectable()
export class SpotifyProvider {

  private token: string;
  private username: string;
  private picture: string;
  private url: string = 'https://api.spotify.com/v1/';

  constructor(public http: Http, private identityProvider: IdentityProvider) {
    this.username = identityProvider.getName ();
    this.picture = identityProvider.getPicture ();
  }

  private buildRequest (resource: string, transformation?: Function){
    let headers = new Headers();
    headers.append('authorization', `Bearer ${this.token}`);
    console.log (`${this.url}${resource}`);
    console.log (this.http.get(`${this.url}${resource}`, {headers: headers}));
    return this.http.get(`${this.url}${resource}`, {headers: headers}).map ((response) => {
      console.log (response);
      return transformation.apply(this, [response.json ()]);
    });
  }

  setInfo (username: string, picture: string) {
    this.username = username;
    this.picture = picture;
    this.identityProvider.setName (this.username);
    this.identityProvider.setPicture (this.picture);
  }

  setToken (token: string) {
    this.token = token;
  }

  getToken () {
    return this.token;
  }

  public getUserInfo() {
    console.log ("CALLED");
    return this.buildRequest ('me', function (user) {
      console.log(user);
      if (user.display_name !== null) {
        this.username = user.display_name;
      } else {
        this.username = user.id;
      }

      console.log (this.username);

      if (user.images.length > 0) {
        this.picture = user.images[0].url;
      } else {
        this.picture = 'assets/img/profile.png';
      }

      this.identityProvider.setName (this.username);
      this.identityProvider.setPicture (this.picture);
      return {
        username: this.username,
        picture: this.picture
      };
    });
  }

  public getUserTracks(next?: string, songs?: Song[]) {
    return this.buildRequest ('me/tracks', function (tracks) {
      let songs: Song[] = [];
      for (const track of tracks.items) {
        songs.push(new Song(track.track.id, this.username, this.picture, track.track.name, track.track.artists[0].name, track.track.album.name, track.track.album.images[0].url, track.track.uri, (new Date()).toISOString().substring(0, 10), track.track.duration_ms));
      }
      return songs;
    });
  }

  public getUserPlaylists () {
    return this.buildRequest ('me/playlists');
  }

  getSongsFromPlaylist (user: string, playlist: string) {
    return this.buildRequest (`users/${user}/playlists/${playlist}/tracks`);
  }

}
