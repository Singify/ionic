import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { IdentityProvider } from '../identity';

@Injectable()
export class FacebookProvider {

  private token: string;
  private username: string;
  private picture: string;
  private url: string = 'https://graph.facebook.com/v2.11/';

  constructor(public http: Http, private identityProvider: IdentityProvider) {
    this.username = identityProvider.getName ();
    this.picture = identityProvider.getPicture ();
  }

  private buildRequest (resource: string, transformation?: Function){
    let headers = new Headers();
    headers.append('authorization', `Bearer ${this.token}`);
    return this.http.get(`${this.url}${resource}`, {headers: headers}).map ((response) => {
      return transformation.apply(this, [response.json ()]);
    });
  }

  setToken (token: string) {
    this.token = token;
  }

  getUserInfo() {
    return this.buildRequest ('me?fields=id,name', function (user) {
      console.log (user);
      if (user.name !== null) {
        this.username = user.name;
      } else {
        this.username = "Anonymous";
      }

      this.picture = `https://graph.facebook.com/${user.id}/picture?type=large`;

      this.identityProvider.setName (this.username);
      this.identityProvider.setPicture (this.picture);
      return {
        username: this.username,
        picture: this.picture
      };
    });
  }
}
