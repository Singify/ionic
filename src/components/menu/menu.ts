import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'menu',
  templateUrl: 'menu.html'
})
export class MenuComponent {
  @Input() activePage: number;
  @Input() isOpen: boolean;

  @Output() onSelection = new EventEmitter<number>();
  @Output() toggle = new EventEmitter<any>();

  constructor() {
  }

  private selectPage(page: number) {
    this.onSelection.emit(page);
  }

  toggleMenu() {
    this.toggle.emit();
  }

}
